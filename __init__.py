"""
Módulo que contiene librerias necesarias para trabajar con flask.
"""
from flask import Flask, jsonify, request, render_template,redirect, url_for

#LIBRERIAS PARA ENVIAR MENSAJES VIA WHTSAPP
from heyoo import WhatsApp
app = Flask(__name__)
mensajes=[]
#EJECUTAMOS ESTE CODIGO CUANDO SE INGRESE A LA RUTA ENVIAR
@app.route("/")
def hello_wordl():
    """
    Esta ruta devuelve la página de inicio en formato HTML.
    """
    return render_template("hello.html")

@app.route("/enviar/", methods=["POST", "GET"])
def enviar():
    """
    Esta ruta se encarga de enviar los mensaje 
    de acuerdo a los datos que coloquemos
    en el formulario. y retorna un json de ok.
    """
    telefono = request.form["numero"]
    mensaje = request.form["mensaje"]
    #TOKEN DE ACCESO DE FACEBOOK
    token='TU TOKEN'
    #IDENTIFICADOR DE NÚMERO DE TELÉFONO
    id_numero_telefono='ID FACEBOOK'
    #TELEFONO QUE RECIBE (EL DE NOSOTROS QUE DIMOS DE ALTA)
    telefono_envia=telefono
    #MENSAJE A ENVIAR
    texto_mensaje=mensaje
    #URL DE LA IMAGEN A ENVIAR
   # urlImagen='https://logowik.com/content/uploads/images/python.jpg'
    #INICIALIZAMOS ENVIO DE MENSAJES
    mensaje_wa=WhatsApp(token,id_numero_telefono)
    #ENVIAMOS UN MENSAJE DE TEXTO
    mensaje_wa.send_message(texto_mensaje,telefono_envia)
    #ENVIAMOS UNA IMAGEN
    #mensajeWa.send_image(image=urlImagen,recipient_id=telefonoEnvia,)
    return jsonify({'respuesta':'ok'})

@app.route('/practica',)
def practica():
    """Esta ruta se encarga de renderizar el html de enviar"""
    return render_template("Enviar.html")
#CODIGO PARA RECIBIR MENSAJES
#CUANDO RECIBAMOS LAS PETICIONES EN ESTA RUTA
@app.route("/recibidos/", methods=["POST", "GET"])
def webhook_whatsapp():
    """
    Esta ruta se encarga de validar el webhook.
    El token "MITOKENPERSONAL" lo generamos nosotros
    con cualquier palabra que deseemos, este token sera necesario para 
    configurar el webhook en la pagina de meta.Tambien se encarga de recibir 
    los mensajes y obtener el número teléfono que envia el mensaje y el texto del mensaje. 
    """
    #SI HAY DATOS RECIBIDOS VIA GET
    if request.method == "GET":
        #SI EL TOKEN ES IGUAL AL QUE RECIBIMOS
        if request.args.get('hub.verify_token') == "MITOKENPERSONAL":
            #ESCRIBIMOS EN EL NAVEGADOR EL VALOR DEL RETO RECIBIDO DESDE FACEBOOK
            return request.args.get('hub.challenge')
            #SI NO SON IGUALES RETORNAMOS UN MENSAJE DE ERROR
        return "Error de autentificacion."
    #RECIBIMOS TODOS LOS DATOS ENVIADO VIA JSON
    data=request.get_json()
    #EXTRAEMOS EL NUMERO DE TELEFONO Y EL MANSAJE
    mensaje="Telefono:"+data['entry'][0]['changes'][0]['value']['messages'][0]['from']
    mensaje=mensaje+"|Mensaje:"+data['entry'][0]['changes'][0]['value']['messages'][0]['text']['body']+" ; "
    #ESCRIBIMOS EL NUMERO DE TELEFONO Y EL MENSAJE EN EL ARCHIVO TEXTO
    mensajes.append(mensaje)
    #RETORNAMOS EL STATUS EN UN JSON
    return jsonify({"status": "success"})

@app.route('/mensajesn', methods=['GET'])
def mostrar_mensajes():
    """
    Esta ruta de encarga de convertir el mensaje recibido en formato json
    """
    # Devolvemos la lista de mensajes en formato JSON
    return jsonify(mensajes)
    #INICIAMSO FLASK
if __name__ == "__main__":
    app.run(debug=True)
