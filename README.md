# ApiCloudWhatsapp
## Descripción general del programa 
El programa usa el api Cloud de WhatsApp, su funcionamiento consiste en una página principal de inicio que tiene un texto tipo link **llamado enviar mensaje**, cuando le damos clic nos lleva a otra página que contiene una zona para enviar mensajes con un formulario sencillo para agregar un número y un mensaje que deseemos, y una zona de lista de mensajes para visualizar los mensajes de respuesta que nos envían. El envío y recepción de mensajes se lo hace sin la necesidad de actualizar la página.
# INSTRUCCIONES
## Configurar cuenta de meta
El api de WhatsApp al pertenecer a la empresa **meta**, nos solicitara un par de requisitos que se nos especifica en el siguiente enlace:  https://developers.facebook.com/docs/whatsapp/cloud-api/, para acceder a la información se debe visitar el sitio **primero pasos** donde nos enseñan como obtener el api, pero, en resumen:
- Deberá registrarse como desarrollador de meta.
- Crear una app de meta donde se le otorgaran credenciales como un token, un numero de prueba etc. 
### enviar mensaje 
Una vez configurada nuestra aplicación de meta con WhatsApp, en la misma pantalla de primeros pasos. En el paso 1: Seleccionar números de teléfono, debemos registrar un numero al que deseemos enviar un mensaje de prueba. 
En el paso 2: Enviar mensajes con la API, nos permite enviar un mensaje al número que hayamos configurado en el paso 1.
### Responder mensaje 
Es importante que el número que recibió el mensaje de prueba, envié una respuesta al mensaje de prueba, para que nuestra aplicación pueda enviar mensajes personalizados. Caso contrario no podremos enviar mensajes personalizados a través de nuestro programa
# Crear servidor
## Configurar servidor  
El uso de un servidor es necesario para la recepción de mensajes ya que meta nos obliga a configurar un weebhook, que deberá tener un enlace tipo https. En ese caso se utilizó el siguiente hosting https://www.alwaysdata.com/
Se van a detallar los pasos para configurar el sitio: 

- Crearse una cuenta con sus credenciales y elegir algún plan, en este caso se utilizó el plan free. 

- En nuestra cuenta visitamos el lugar sites, por defecto nos trae un sitio preinstalado, 
debemos copiar el nombre del sitio que será algo parecido a **su_nombre_usuario**.alwaysdata.net 
y guardarlo en un bloc de notas, por ejemplo. Finalmente podemos borrar ese sitio 

- Para crear un nuevo sitio, daremos clic sobre **install an application** y elegiremos la opción **Flask**. 

### Configuración del sitio web 

- En **addresses** colocaremos el nombre del sitio que habíamos copiado antes. 
- En **installation** colocaremos ´/www3/´. Y aceptamos las condiciones 

Así ya tenemos nuestro sitio web con certificado https.

# Descarga y configuración de archivos 
## Descargar

Para poder descargar los archivos podemos clonar el repositorio en alguna carpeta de tu computador: 
1.	Haz clic en el botón "Clone" para copiar la URL del repositorio.
2.	Abre una terminal en tu computadora y navega hasta el directorio donde deseas clonar el repositorio.
3.	Ejecuta el comando git clone:
```
git clone https://gitlab.com/Keviing1/apicloudwhatsapp.git
```

## Configuración de archivos

### Configurar el envío de mensajes
Una vez que descargamos los archivos de repositorio de GitLab, abriremos la carpeta con nuestro editor de código preferido para realizar algunas modificaciones en el código.
Las modificaciones únicamente se realizarán en el archivo __init__.py 
En la siguiente ruta: 
```
@app.route("/enviar/", methods=["POST", "GET"])

```
Encontraremos esta sección de código: 
```
    token='TU TOKEN'
    idNumeroTeléfono='ID FACEBOOK'
```
Que son variables que contienen nuestro token y el id de nuestro número de prueba, esta información nos la proporciona meta cuando creamos nuestra aplicación meta debemos copiar el token de acceso temporal y el identificador de número de teléfono. 
## Configurar la recepción de mensajes
### Configurar archivo __init__.py para recibir los mensajes
Otra configuración que debemos realizar en el mismo archivo __init__.py es el siguiente.
En la siguiente ruta: 
```
@app.route("/recibidos/", methods=["POST", "GET"])

```
Encontraremos esta sección de código: 
```
if request.args.get('hub.verify_token') == " MITOKENPERSONAL"
```
Aquí colocaremos un token personal que nosotros generemos esto nos servirá mas adelante para verificar que nuestra página es segura. 
## Conectarnos al servidor 
Una vez que tenemos nuestro sitio web y archivos configurados, debemos conectarnos a nuestro servidor para poder subir nuestros archivos para que estos se alojen en nuestro hosting.
Para ello deberemos instalar la aplicación FileZilla en el siguiente enlace: https://filezilla-project.org/download.php?type=client .
Una vez instalado FileZilla abrimos el programa y debemos seguir los siguientes pasos para subir los archivos: 

1. En el sitio alwaysdate.net accedemos **Remote access**>**FTP** y observamos nuestro 
name y nuestro FTP host: ftp-tuUsuario.alwaysdata.net.

2. Dentro de la aplicación **FileZilla** accedemos en la parte superior izquierda en **archivo > gestor de sitios**.

3. Para hacer la conexión en el campo **servidor:** colocaremos nuestre FTP host que en este 
ejemplo es **ftp-TuUsuario.alwaysdata.net**. 

4. En **usuario** colocaremos el name que nos proporciona alwaysdata y 
en **contraseña** colocaremos la contraseña con la que iniciamos sesión. 

5. Damos clic en **conectar**, y tendremos dos pestañas la izquierda que nos muestra nuestras carpetas locales 
y la derecha que nos muestra nuestra carpeta de nuestro sitio remoto.

## Subir los archivos al servidor remoto 

Para subir los archivos del sitio local al servidor remoto, haremos lo siguiente: 


1. En el sitio local debemos ubicarnos sobre la carpeta que contiene nuestros archivos descargados

2. En el sitio remoto debemos ubicarnos en la carpeta www3

3. Ambos sitios deberán tener un archivo con el nombre __init__.py , el archivo del sitio local 
lo debemos arrastrar hacia el lado del sitio remoto, ahí nos indicara que el archivo ya existe
le daremos clic a reescribir.

4. De igual forma ambos sitios deben tener una carpeta con el nombre de templates  accederemos al contenido de ellas 
dando doble clic sobre las carpetas, y de la misma  forma arrastraremos los archivos html del sitio local al sitio remoto 
y elegiremos  la opción de reescribir en ambos casos.

Una vez subimos todos los archivos a nuestro servidor, en la página alwaysdata nos colocamos en la pagina **sites** y damos clic en el boton de restart para refrescar todo el servidor. realizaremos las ultimas configuraciones para que todo funcione correctamente.

## Instalar librerías de heyoo 
Como pudiste haber notado se utiliza heyoo para poder utilizar el api de WhatsApp, estas librerías las debemos instalar en nuestro servidor para que el programa funcione correctamente, y eso lo realizaremos de la siguiente manera: 
En la pagina de alwaysdata.net, accedemos a la dirección: Remote Access > SSH
Una vez estemos en la pagina SSH, por defecto en el campo **Password Login** está configurada como **no** , en este caso primero configuraremos una contraseña sobre el icono de edit que tiene forma de rueda dentada.
Configuramos el password con una contraseña que deseemos o podemos colocar la misma contraseña con la que iniciamos sesión. Luego marcamos la opción de “enable password login”.
Después sobre en la pagina se SSH, en la parte superior encontrásemos una línea similar a esta: 
```
SSH host: ssh-tucusuariio.alwaysdata.net (also Web accessible)
``` 
Damos clic sobre Web.
Ahí colocaremos nuestro usuario y nuestra contraseña que acabamos de configurar, como se muestra en el siguiente ejemplo:
```
ssh1 login: tuUsuario                                                                 
Password: tuContraseña
                                                                          
Linux ssh1 5.15.74-alwaysdata #20221004 SMP Thu Oct 20 12:06:30 UTC 2022 x86_64    

  * Any process using too much CPU, RAM or IO will get killed                                                                                                           
  * Any process running for too long (e.g. days) will get killed                                                                                                        
  * If you want to have cron jobs, use scheduled tasks: https://help.alwaysdata.com/en/tasks/                                                                           
  * If you want to have processes running 24/7, use services: https://help.alwaysdata.com/en/services/                                                                  

Last login: Fri Mar 31 22:03:14 2023 from 2a00:b6e0:1:50:1::1 

```
Luego ejecutaremos las siguientes líneas de comando para instalar heyoo: 

```
cd www3

source env/bin/activate 

pip install heyoo  
                                      
```
Listo con esto ya tenemos instalado las librerías de heyoo. Luego en la página **sites** de alwaysdata damos clic en el boton de restart para refrescar todo el servidor.

Finalmente debemos configurar nuestro webhook.  

### Configurar webhook para recibir mensajes

Debajo de la sección primeros pasos tenemos la sección de configuración, dentro de esta sección tenemos el apartado de webhook, damos clic sobre editar y realizaremos las siguientes configuraciones: 
URL de devolución de llamada: 
```
Debemos colocar nuestra dirección https con la ruta que recibe los mensajes, como se muestra en el siguiente ejemplo:
*Se debe colocar a nuestra url la ruta /recibidos/ ya que esta es la ruta encargada de validar nuestro webhook. 
https://tuurl.alwaysdata.net/recibidos/

```
Token de verificación
```
En el token de verificación debemos colocar el token personal que nosotros generamos 
en nuestro archivo __init__.py que en este ejemplo fue el siguiente: 

MITOKENPERSONAL
```
Así es como podremos configurar y poner en funcionamiento nuestro programa. 

# Información relevante

- Dentro del formulario en el campo de número, el numero que podremos colocar sera el numero al que enviamos el mensaje de prueba.
- El numero debera tener el formato de codigo de pais por ejemplo para Ecuador **593** y el resto del numero sin el **0** por ejemplo: 
para el numero 0985473652 el numero que colocaremos en el formulario sera : **593985473652**
- El mensaje de respuesta se observe 5 segundos después de que se envío el mensaje.  

**Nota:** El programa se analizo de forma estatica utilizando pylint
Para installar pylint ejecute la siguiente linea de comandos:
```
pip install pylint
```
Para analizar estaticamente nuestro proyecto ejecutamos la siguiente linea de comandos: 
```
pylint __init__.py
```
